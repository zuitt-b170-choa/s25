// 1

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$onSale", fruitsOnSale: {$sum: 1}}},
    {$project: {_id: 0}}
])

// 2

db.fruits.aggregate([
    {$match: {stock: {$gt: 20}}},
    {$group: {_id: "stock", enoughStock: {$sum: 1}}},
    {$project: {_id: 0}}
])

// 3

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", avg_price: {$avg: "$stock"}}},
    {$sort: {_id: 1}}
])

// 4

db.fruits.aggregate([
    {$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
    {$sort: {_id: 1}}
])

// 5

db.fruits.aggregate([
    {$group: {_id: "$supplier_id", min_price: {$min: "$price"}}},
    {$sort: {_id: 1}}
])
